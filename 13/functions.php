<?php
//saját eljárások
//hiba kiírásra
function hibaKiir($key, &$hiba){//hiba változó referenciaátadása
    //ha nem létezik a hiba tömb, (array_key__existnek az kell!) akkor létrehozunk üreset, ha létezik akkor nem változtatjuk
    $hiba = $hiba?:[];
    //ha létezik az adott kulcs akkor visszaadjuk a rajta található értéket
    if(array_key_exists($key,$hiba)){
        return $hiba[$key];
    }
    return;
}