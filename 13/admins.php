<?php
require_once('functions.php');//saját eljárások
require_once('config.php');//beállítások
require_once('connect.php');//db csatlakozás -> $link

$action = filter_input(INPUT_GET, 'act') ?: 'list';//művelet, vagy sikerül filterezni, vagy list
$t_id = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);// id ha van
$dbTable = 'admins';
//var_dump($action, $t_id);

//post ellenőrzés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //username kötelező
    $username = trim(filter_input(INPUT_POST, 'username'));
    if (!$username) {
        $hiba['username'] = '<span class="error">Kötelező mező!</span>';
    }
    //email kötelező és email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if ($email === false) {//érték és tipus egyezés
        $hiba['email'] = '<span class="error">Nem megfelelő formátum!</span>';
    }else{
        //email cim unique!
        $qry = "SELECT id FROM $dbTable WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if($row[0] != $t_id && $row[0] !== NULL){//update miatt módosítottuk hogy a saját emailje ne dobjon hibát
            $hiba['email'] = '<span class="error">Foglalt email!</span>';
        }
    }
    //jelszó kötelező és min 6 karakter, és az update miatt legalább 1 karakter legyen az 1. input mezőben hogy módosításkor ne kelljen jelszót megadni mindig, csak ha változtatni akarjuk
    $password = filter_input(INPUT_POST, 'pass');
    if($action == 'new' || ($action == 'update' && mb_strlen($password, 'utf-8') > 0 )) {
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['pass'] = '<span class="error">Nem megfelelő hossz (min 6 karakter)!</span>';
        } //jelszó megerősítés mező értéke egyezzen a pass mező értékével, de csak akkor ha a hossz megfelelő (elseif())
        elseif ($password != filter_input(INPUT_POST, 'repass')) {
            $hiba['repass'] = '<span class="error">Jelszavak nem egyeztek!</span>';
        } else {
            //jelszó titkosítása!!! soha NE TÁROLJ KÓDOLATLAN JELSZÓT!!!!!
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }
    if (empty($hiba)) {//nincs hiba

        $data = [
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'status' => 1,
            'time_created' => date('Y-m-d H:i:s')
        ];//adatok 'rendberakása' , előkészítése a további műveletekhez

        if($action == 'new') {
            //új felvitel
            $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($data)) . "`) VALUES('" . implode("','", $data) . "')";//összeállítjuk az insertet a data segédtömb eleminek segítségével
            //'A felvitt admin azonosítója: '.mysqli_insert_id($link);
        }else{//update
            $qry = "UPDATE $dbTable SET 
                    username = '{$data['username']}',
                    email = '{$data['email']}',
                    status = {$data['status']}
                    WHERE id = $t_id ";
        }
        //query futtatása
        mysqli_query($link,$qry) or die(mysqli_error($link));

        $insert_id = $t_id?:mysqli_insert_id($link);//ha update akkor tudjuk, ha insert akkor megkapjuk

        header('location:?act=listvagybármi');
        exit();
    }
}
$output = '';//ez lesz a modul kimenete
switch ($action) {
    case 'delete':
        //echo 'törlünk (id) ' . $t_id;
        if($t_id > 0){
            $qry = "DELETE FROM $dbTable WHERE id= '$t_id' LIMIT 1";
            mysqli_query($link,$qry) or die(mysqli_error($link));
            //törlés után vissza a listára
            header('location:?act=list');
            exit();
        }
        break;

    case 'update':
        if($t_id > 0){//azonosító alapján lekérjük a megfelelő sort
            $qry = "SELECT id,username,email,status FROM $dbTable WHERE id= '$t_id' LIMIT 1";
            $result = mysqli_query($link,$qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
        }
        var_dump($row);
        //break;

    case 'new':
        //echo 'új felvitel űrlap+hibák ha van';
//ha nincs row a db-ből akkor üres tömb kell legyen a saját getInputValue eljárás paraméterezése érdekében
        $row = isset($row)?$row:[];
        //űrlap összeállítása
        $form = '<form method="post">
    <div class="form-group">
        <label>Felhasználónév<sup>*</sup>:
            <input type="text" name="username" placeholder="username" value="' . getInputValue('username',$row) . '">' . hibaKiir('username', $hiba) .
            '</label>
    </div>
    <div class="form-group">
        <label>Email<sup>*</sup>:
            <input type="text" name="email" placeholder="email@cim.hu" value="' . getInputValue('email',$row) . '">' . hibaKiir('email', $hiba) .
            '</label>
    </div>
    <div class="form-group">
        <label>Jelszó<sup>*</sup>:
            <input type="text" name="pass" value="">' . hibaKiir('pass', $hiba) .
            '</label>
    </div>
    <div class="form-group">
        <label>Jelszó megerősítése<sup>*</sup>:
            <input type="text" name="repass" value="">' . hibaKiir('repass', $hiba) .
            '</label>
    </div>
    <button>Admin felvitele</button>
</form>';
        $output .= $form;
        break;
    default://listázás
        $qry = "SELECT id,username,email,status FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC );
        //tábla kezdés és új felvitel 'gomb'
        $output .= '<div><a href="?act=new">új felvitel</a></div>
                <table>';
        //fejléc
        $output .= '<tr>';
        $output .=  "<th>id</th>"
            ."<th>username</th>"
            ."<th>email</th>"
            ."<th>status</th>"
            ."<th>művelet</th>";
        $output .= '</tr>';
        //adatsorok
        foreach($rows as $admin) {
            $output .= '<tr>';
                $output .=  "<td>{$admin['id']}</td>"
                            ."<td>{$admin['username']}</td>"
                            ."<td>{$admin['email']}</td>"
                            ."<td>{$admin['status']}</td>"
                            ."<td><a href='?act=update&amp;tid={$admin['id']}'>módosít</a> | <a href='?act=delete&amp;tid={$admin['id']}'>töröl</a></td></td>";
            $output .= '</tr>';
        }
        $output .= '</table>';
        //echo '<pre>' . var_export($rows, true) . '</pre>';

}

echo $output;//kimenet kiírása egy lépésben

function getInputValue($name , $row ){//fallback: post,db érték, semmi

    $post_input = filter_input(INPUT_POST,$name);

    if( $post_input !== NULL ){
        return $post_input;
    }
    //ha nincs post, nézzük a db-t, ha a rownak van megfelelő kulcsa akkor jött adat a db-ből
    if(array_key_exists($name,$row)){
        return $row[$name];
    }
    //egyébként meg semmi
    return;
}