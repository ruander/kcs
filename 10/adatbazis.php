<pre><?php
//szerver adatok
$db_host = 'localhost';
$db_user = 'root';
$db_password = '';
$db_name = 'classicmodels';

//csatlakozás felépítése
$link = @mysqli_connect($db_host, $db_user, $db_password, $db_name);
mysqli_set_charset($link, 'utf8');//aktuális kódlap illesztése a felépített linkre
//var_dump(mysqli_connect_error());die('itt');//a connect hibaüzenetét tartalmazza
if (!$link) {
    die('DB hiba!');
}
//var_dump($link);//resource object

//lekérések összeállítása a queryhez
$qry = 'SELECT * FROM employees';
//lekérés képzése eredmény változóba
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//eredmény(ek) feldolgozása
//var_dump($result);//halmaz
var_dump(mysqli_fetch_row($result));
var_dump(mysqli_fetch_assoc($result));
var_dump(mysqli_fetch_array($result));
//ciklus az eredmények kibontására
while ($row = mysqli_fetch_assoc($result)) {
    var_dump($row);//aktuális elem a rowban van
    var_dump($row['employeeNumber']);//egy érték kiírása
}
//pl egy táblázat kiírása egy lekérésből
//lekérés
$qry = "SELECT officeCode, city, country FROM offices";//query összeállítása
$result = mysqli_query($link, $qry) or die(mysqli_error($link));//lekérés

$table = '<table border="1">';
$thead = '';
$tcontent = '';
while ($row = mysqli_fetch_assoc($result)) {
    //cimsor(ok)
    if ($thead == '') {//ha eddig nem volt fejléc akkor kialakítjuk
        $thead .= '<tr><th>' . implode('</th><th>', array_keys($row)) . '</th></tr>';
    }
    //adatsor kialakítása
    $tcontent .= '<tr><td>' . implode('</td><td>', $row) . '</td></tr>';
}
$table .= $thead;//kialakított címsor táblázatba fűzése
$table .= $tcontent;//adatsorok illesztése a table -be
$table .= '</table>';
echo $table;
//új sor beillesztése
/**
 * INSERT INTO tablename(mezo1,mezo2,mezo3,...) VALUES(value1,value2,value3,...)
 */
$insert = mysqli_query($link, "INSERT INTO employees(
                                            `employeeNumber`,
                                            `lastName`,
                                            `firstName`,
                                            `extension`,
                                            `email`,
                                            `officeCode`,
                                            `reportsTo`,
                                            `jobTitle`)
                                        VALUES(
                                            7000,
                                            'Horvath',
                                            'George',
                                            '7007',
                                            'hgy@iworkshop.hu',
                                            '1',
                                            1002,
                                            'Mindenes')"
) or die (mysqli_error($link));
var_dump($insert);
$update = mysqli_query($link, "UPDATE employees
                                    SET firstName = 'György' 
                                    WHERE employeeNumber = 7000 
                                    LIMIT 1") or die (mysqli_error($link));
var_dump($update);
//sor törlése (amit feljebb létrehoztunk majd módosítottunk
$delete = mysqli_query($link, "DELETE FROM employees WHERE employeeNumber = 7000 LIMIT 1") or die (mysqli_error($link));
var_dump($delete);
$employee = [
    'employeenumber' => 7000,
    'firstname' => 'George',
    'lastname' => 'Horváth',
    'extension' => '7007',
    'email' => 'hgy@ruander.hu',
    'officecode' => '7',
    'reportsto' => 1002,
    'jobtitle' => 'Mindenes'
];
//ellenőrizzük létezik e már az elem a primary key(ek) alapján
echo $qry = "SELECT * FROM employees WHERE employeenumber = {$employee['employeenumber']} LIMIT 1";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
if (mysqli_num_rows($result) == 0) {//ha nincs ilyen sor még akkor insert
    var_dump($row);
//insert kérés összeállítása
    $qry = "INSERT INTO employees(`" . implode("`,`", array_keys($employee)) . "`) 
VALUES('" . implode("','", $employee) . "')";
//lekérés futattása
    $insert = mysqli_query($link, $qry) or die(mysqli_error($link));
}
//újra lekérjük az összes alkalmazottat
$qry = "SELECT * FROM employees";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//lezárjuk az adatbázis kapcsolatot
mysqli_close($link);
//egy lépésben kibontunk mindent
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
var_dump($rows);




