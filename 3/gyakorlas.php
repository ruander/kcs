<?php
/**
 * több soros komment
 * prim keresés tól - ig
 */
$tol = 2;
$ig = 1000000;
$primek = [];//itt tároljuk a primeket
$start = microtime(true);
for($i=$tol;$i<=$ig;$i++){
    //az aktuális számnak keressük az osztóit, a szabály miatt a szám gyökéig
    if(isPrim($i)) {
        $primek[] = $i;
    }
}
$stop = microtime(true);
echo 'Futási idő: '.($stop-$start).' s';
echo implode(', ',$primek);
/*$is_prim = true;
$szam = 42;
$oszto=2;
while( $is_prim == true
        and
        $oszto <= sqrt($szam) ){
        if($szam%$oszto == 0) $is_prim = false;//csonka if, csak 1 utasitást hajtunk végre és csak igaz ágban
        $oszto++;
}
$is_prim = isPrim(42);
var_dump($is_prim);*/
//gyártsunk belőle eljárást
/**
 * isPrim
 * @todo return false when $szam not integer or  lower than 2
 * @param int $szam
 * @return bool
 */
function isPrim($szam){
    $is_prim = true;
    $oszto=2;
    while( $is_prim == true
        and
        $oszto <= sqrt($szam) ){
        if($szam%$oszto == 0) $is_prim = false;//csonka if, csak 1 utasitást hajtunk végre és csak igaz ágban
        if($oszto == 2) {//a páros számokkal való osztást a 2 már kizárta
            $oszto++;
        }else{
            $oszto+=2;
        }
    }
    return $is_prim;//bool
}