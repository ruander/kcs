<?php
$anotherMenu = [
    1 => [
        'title' => 'Kezdőlap',
        'slug' => 'home',
        'submenu' => false,
        'icon' => 'fa fa-home',
    ],
    2 => [
        'title' => 'Rólunk',
        'slug' => 'about',
        'submenu' => false,
        'icon' => 'fa fa-user',
    ],
    3 => [
        'title' => 'Szolgáltatások',
        'slug' => 'services',
        'submenu' => [
            1 => [
                'title' => 'Oktatás',
                'slug' => 'education',
                'submenu' => false,
                'icon' => 'fa fa-item1',
            ],
            2 => [
                'title' => 'Tanácsadás',
                'slug' => 'consultancy',
                'submenu' => false,
                'icon' => 'fa fa-item2',
            ],
            3 => [
                'title' => 'Refaktorálás',
                'slug' => 'refactoring',
                'submenu' => false,
                'icon' => 'fa fa-item3',
            ],
        ],
        'icon' => 'fa fa-search',
    ],
    4 => [
        'title' => 'Kapcsolat',
        'slug' => 'contact',
        'submenu' => false,
        'icon' => 'fa fa-email',
    ],
];

$navigation = '<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
    <ul class="navbar-nav">';

//bejárjuk a menü tömb első szintjét és ha az adott elem dropdown kulcsa false akkor felépítjük a dropdown nélküli menüpont elemet dinamikus adatokkal
foreach($anotherMenu as $itemId => $data){
    if($data['submenu']==false) {
        //dropdown nélküli menüpont
        $navigation .= '<li class="nav-item">
        <a class="nav-link" href="#'.$data['slug'].'">'.$data['title'].'</a>
      </li>';
    }elseif(is_array($data['submenu'])){//egyébként ha tömböt találunk felépítjük a dropdown hordozót
        //dropdown hordozó menüpont
        $navigation .= '<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$data['title'].'</a>
        <div class="dropdown-menu" aria-labelledby="dropdown08">';
        foreach($data['submenu'] as $subId => $subData) {
            $navigation .= '<a class="dropdown-item" href="#'.$subData['slug'].'">'.$subData['title'].'</a>';//submenuitem beillesztése
        }
        $navigation .= '</div>
      </li>';//dropdown hordozó menupont lezárás
    }
}// menu elemek készen vannak
$navigation .= '</ul>
  </div>
</nav>';
echo $navigation;