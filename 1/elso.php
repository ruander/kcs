<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>1. php fileom</title>
</head>
<body>
<?php
//hello world! - egysoros komment
/*
több
soros 
komment
*/
echo 'Hello World!';// operátor: '' , "" - string határoló

//változók - primitíve
$szam = 5;// operátorok: = -> értékadó , $ -> változó; típusa: integer
echo '<br>';
print $szam;
$szoveg = "3 Horváth György";// típus: string
$logikai = true; // boolean vagy bool
$lebegoPontos = 3.14;//floating poiint vagy floating

//változók állapotának kiírása
echo '<pre>';
var_dump($szam,$szoveg,$logikai,$lebegoPontos);
echo '</pre>';
echo $a;//nem létező változó használata -> notice

//automatikus tipuskonverzió
$sum = $szam + $szoveg;//operátor + -> összeadás
var_dump($sum);
$eredmeny = $lebegoPontos.$szoveg;//operátor: . -> konkatenátor
var_dump($eredmeny);

//gyakorlás
$dobas = rand(1,6);
echo '<br>A dobás értéke: $dobas';
echo "<br>A dobás értéke: $dobas"; //"" belül default értelmezi a nyelvi elemeket
echo '<br>A dobás értéke: $dobas';
echo "<br>A dobás értéke: \$dobas";//operátor: \ - escape , a következő nyelvi elemet kilépteti a végrehajtásból

echo '<br>A dobott szám ('.$dobas.'), ami ';

//vizsgálat
/* if(condition){
	true
}else{
	false
}	
*/
if($dobas%2 == 1){
	echo ' páratlan.';
}else{
	echo 'páros.';
}
$dobas2 = rand(1,6);
$sum = $dobas + $dobas2;
echo "<br>A dobások összege: $dobas + $dobas2 = $sum ";
echo '<pre>';
//tömbök (array)
$tomb = [];//üres tömb = > array();
$tomb = [2019, 'Horváth György', $lebegoPontos, true, false];
$tomb[100] = 'ez a 100 index';//irányított indexre illesztés
$tomb["email"] = 'hgy@iworkshop.hu';//asszociatív elem
$tomb[] = 5*4;//101 kulcsra kerül

var_dump( $tomb, count($tomb));
//ciklusok
//dobjunk 3x kockával és mondjuk meg a dobások összegét
$sum = 0;
/*
for(ciklusváltozó kezdeti értéke; belépési feltétel; ciklusváltozó léptetése){
		ciklusmag
}
*/
for($i=1;$i<=3;$i++){
	$sum = $sum + rand(1,6);
}
echo '<h3>A dobások összege: '.$sum.'</h3>';
//tombot használunk a dobások tárolására
$dobasok = [];
while(count($dobasok)<3){
	$dobasok[]=rand(1,6);
}
echo '<h3>A dobások összege: '
.array_sum($dobasok)
.' ('.
implode(',' , $dobasok)
.')</h3>';

//tömb bejárása
foreach($tomb as $k => $v){
	echo "<br>Aktuális index: $k , aktuális érték: $v";
}
//tomb megadás irányított indexel
$userdata = [
	'id' => 5,
	'email' => 'hgy@iworkshop.hu',
	'regTime' => time(),
	'otherdata' => [
		'valami' => 'bármi',
		12 => rand(1,100)
	],
	'regDate' => date('Y-m-d H:i:s'),
];
var_dump($userdata);

?>

</body>
</html>