<pre><?php
//folyamatábra alapján!
//5/90 ből paraméterezhető lottoszám generálás

$limit = 90;
$huzasok_szama = 5;
$tippek = [];//ide gyűjtjük a tippeket
//kezdeti paraméterek ellenőrzése:
if($huzasok_szama>$limit){
    die('Paraméterezési hiba!');//itt megáll a kód mint a bot! exit = die
}
while(count($tippek)<$huzasok_szama){
    $tipp = rand(1,$limit);
    if(!in_array($tipp,$tippek)){//php.net!
        $tippek[]=$tipp;
    }
}
//rendezés érték szerint emelkedő sorrendbe, a tömbbe nyúl bele az eredetit módosítja!
sort($tippek);
var_dump($tippek);



//2. verzio functionnel
$tippek_maskepp = sorsolas(5,90);
var_dump($tippek_maskepp);
//újra felhasználható változat, egy kicsit másképpen (unique elemek a halmazban!)
function sorsolas($huzasok_szama = 5, $limit = 90){
    //hibakezelés
    if($huzasok_szama>$limit){
        die('Paraméterezési hiba!');//itt megáll a kód mint a bot! exit = die
    }
    //elvileg jo
    $tippek = [];
    while(count($tippek)<$huzasok_szama){
        $tippek[]=rand(1,$limit);
        //array_unique 7.2 től változott a példánkban a régi változat bemutatása van
        $tippek = array_unique($tippek,SORT_REGULAR);//eltároljuk önmaga unique elemekkel rendelkező változatát
    }
    //rendezünk
    sort($tippek);
    //megvan a tömb, visszatérünk vele
    return $tippek;
}