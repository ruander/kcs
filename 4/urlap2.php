<?php
var_dump($_POST);
//szuperglobális tömböt soha, ismétlem SOHA ne használj közvetlen kulcsokról

if(!empty($_POST)){//csak akkor használjuk a post tömböt ha van benne valami
    //var_dump($_POST['name']);// <--- igen igen EZT NEM ILLIK!
    //hibakezelés
    $hiba=[];//üres hibatömb

    //name ellenőrzése
    $name=filter_input(INPUT_POST,'name');// <-- így kéne
    if($name == ''){
        $hiba['name']='<span class="error"> &lt; -- tőccsed ki!</span>';
    }
    //email ellenőrzése
    $email=filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);// <-- így kéne
    if($email == ''){
        $hiba['email']='<span class="error"> &lt; -- invalid format!</span>';
    }
    //ha a hiba kezelés során üres maradt a hibatömb akkor minden OK
    if(empty($hiba)){
        //die( 'Királyság van!' );
    }

}
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Űrlapok 2</title>
</head>
<body>
<h1>A zűrlapok és feldolgozásuk</h1>
<h2>2. feldolgozás ugyanabban a fileban</h2>
<form method="post">
    <label>Név<sup>*</sup>
    <input type="text" name="name" placeholder="Nameless One" value="<?php
        //shorten if: condition ? true : false
    echo isset($name) ? $name : '';
    //echo filter_input(INPUT_POST,'name')?:''; //itt nincs a true ág a condition eredménye maga
    ?>">
        <?php
        //hiba kiírása, ha létezik
        if(isset($hiba['name'])) echo $hiba['name'];
        ?>
    </label>
    <!--email cím bekérése-->
    <label>Email<sup>*</sup>
        <input type="text" name="email" placeholder="email@cim.hu" value="<?php
        echo filter_input(INPUT_POST,'email')?:'';
        ?>">
        <?php
        //hiba kiírása, ha létezik
        if(isset($hiba['email'])) echo $hiba['email'];
        ?>
    </label>

    <br><input type="submit" value="gyííí" name="submit">
</form>
</body>
</html>