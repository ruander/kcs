<?php
//teljes lottójáték megvalósítása
/**
 * Választ
 * tárolandó adatok:
 * email -> ez azonosítja a tippelőt
 * tippsor -> játéktipustól függő elemszám
 * mappa: tippek/játéktipus(5,6,7)/hét(18)/tippek.json
 * sorsolás gombnyomásra egy külön fileba, ha még nem volt
 * nyertesek eltárolása ugyanoda winners.json néven
 */
//erőforrások - resources
$gameTypes = [
    5 => 90,
    6 => 45,
    7 => 35,
];//lehetséges játéktipusok
$dir = 'tippek/';//tippek mappája
$week = date('W');//aktuális hét
$ext = '.json';//file kiterjesztése
$includeDir = 'includes/';//alkotóelemek mappája
$output = '';//kiírandó string a folyamat végén
//url paraméterek
$gameType = filter_input(INPUT_GET,'g');
//ha van akkor ellenőrízzük hogy jó-e, azaz benne van e a gametypes tömbben a kulcs
if($gameType != ''){
    if(!array_key_exists($gameType,$gameTypes)){
        $output .= 'Nepiszkáld az url-t mert elvisznek a TEKesek!';
        $gameType = false;
    }else{
        $dir .= $gameType.'/';
    }

}
/////////////////
if (!empty($_POST)) {
    $hiba = [];
    $limit = $gameTypes[$gameType];
    //mezők hibakezelése
    $tippek = filter_input(INPUT_POST,'tippek',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    $egyedi_tippek = array_unique($tippek);//az ismétlődés ellenőrzésére
    //egyesével ellenőrizzük a tippeket
    $hiba['tippek'] = [];//tipp mezők hibájának előkészítése, ezt ha üres marad törölni kell a folymamat végén!
    foreach($tippek as $k => $v){
        //tipp egész számra alakítása
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => $limit,
            ]
        ];
        $v = filter_var($v,FILTER_VALIDATE_INT, $options);
        //limit ellenőrzése
        if($v == false){
            $hiba['tippek'][$k] = '<span class="error"> Rossz adat!</span>';
        }else{//limit jó, ismétlődés keresése
            if(!array_key_exists($k,$egyedi_tippek)){
                $hiba['tippek'][$k] = '<span class="error"> Már tippelted!</span>';
            }
        }
    }
    if(empty($hiba['tippek'])) unset($hiba['tippek']);
    //tippellenőrzés vége
    //email ellenőrzése
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
    if(!$email){
        $hiba['email'] = '<span class="error"> Nem jó formátum!</span>';
    }
    if(empty($hiba)) {
        //szükséges mappa ellenőrzése/létrehozása
        $current_dir = $dir.$week;//tippek/18
        if(!is_dir($current_dir)){
            mkdir($current_dir,0755, true);
        }
        //file beolvasása ha van
        $fileName = 'tippek';
        $fullFileName=$current_dir.'/'.$fileName.$ext;//tippek/18/tippek.json
        if(file_exists($fullFileName)){
            $oldTippek = json_decode(file_get_contents($fullFileName));
        }else{
            $oldTippek=[];
        }
        //'letisztázzuk' a tárolni kívánt adatokat
        $data = [
            'email'=>$email,
            'tippek'=>$tippek,
        ];
        //beillesztjük az eddigi edatokhoz az új adatcsoportot
        $oldTippek[]=$data;
        //kiírjuk a filet encode-olva
        file_put_contents($fullFileName,json_encode($oldTippek));
        //'visszairányítjuk a filet a játék kezdetéhez
        header('location:index.php');exit();
    }
}
include($includeDir.'header.php');//html nyitások és a head rész + body nyitás
if(!$gameType) {
    include($includeDir . 'navigation.php');//játékválasztó menü
}else{//van gametype jöhet a Zűrlap
    include($includeDir . 'form.inc');
}
include($includeDir.'footer.php');// body+ html zárás
