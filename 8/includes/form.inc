<?php
//bármilyen tipusú játék űrlap elemei
$form = '<form method="post">';
for($i=1;$i<=$gameType;$i++){
    $form .='<label>Tipp '.$i.': <input type="text" name="tippek[]" value="'.(isset($tippek[$i-1])?$tippek[$i-1]:'').'" size="2" maxlength="2" placeholder="'.$i.'">';
    $form .= (isset($hiba['tippek'][$i-1]) ) ? $hiba['tippek'][$i-1] : '';//hiba kiírás ha van
    $form .= '</label>';
}
//email mező
$form .='<label>Email<sup>*</sup>: <input type="email" name="email" value="'.filter_input(INPUT_POST,'email').'" placeholder="email@cim.hu">';
$form .= isset($hiba['email']) ? $hiba['email'] : '';//hiba kiírás ha van
$form .= '</label>';
//küldés gomb
$form .='<button name="submit" value="ok">Tippelek</button>';
$form .='</form>';
echo $form;//űrlap kiírása