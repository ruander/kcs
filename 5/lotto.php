<pre><?php
//erőforrások felvétele
$limit = 90;
$huzasok_szama = 5;

//ha van post adat, hibakezelünk
if (!empty($_POST)) {
    $hiba = [];//üres hiba tömb
    //hibakezelés
    $options = [ //options tömb összeállítása a filternek
        'options' => [
            'min_range' => 1,
            'max_range' => $limit,
        ]
    ];
    //tippek 'kiszűrése' a postból
    $tippek_filter = filter_input_array(INPUT_POST,[
        'tippek'    => [
            'flags'  => FILTER_REQUIRE_ARRAY,
        ]
    ]);
    $tippek = $tippek_filter['tippek'];//csak a tippek átvétele a tippek tömbbe
    $tippek_unique = array_unique($tippek);//egyedi tippek tömbje, ami kulcs itt nincs, az ismétlődés miatt kikerült
    //var_dump($tippek,$tippek_unique);
    //tippek tömb hibakezelése
    foreach($tippek as $k => $v){
        if($v==''){//ha üres a mező akkor ezt írjuk ki
            $hiba['tippek'][$k]= '<span class="error">Kötelező kitölteni!</span>';
        }else{
            //formátum ellenőrzés
            if(!filter_var($v,FILTER_VALIDATE_INT, $options)){
                $hiba['tippek'][$k]= '<span class="error">Hibás formátum!</span>';
            }

            //ismétlődés ellenőrzése -> ha a tippek_unique szűrt tömbböl hiányzik...
            if(!array_key_exists($k,$tippek_unique)){
                if(!isset($hiba['tippek'][$k])){//ha eddig üres volt akkor hogy legyen mihez hozzáfűzni előkészítjük a változót hogy később a hozzáfűzés ne okozzon noticet
                    $hiba['tippek'][$k]='';
                }
                $hiba['tippek'][$k] .= '<span class="error">Már tippelted!</span>';
            }
        }
    }

    if (empty($hiba)) {
        sort($tippek,SORT_REGULAR);//növekvő sorrendben
        $tippek_string = implode(',',$tippek);//tippek vesszővel elválsztva
        //egyszerű tárolás fileba
        $filename = 'tippek.txt';
        file_put_contents($filename,$tippek_string);
        die('nincs hiba!');
    }
}

$form = '<form method="post">';//form open

//ciklus a mezőknek - HUZÁSOK SZÁMÁNYI mező
for($i=1;$i<=$huzasok_szama;$i++) {
    $form .= '<br><label>Tipp '.$i.'<sup>*</sup>: <input type="text" name="tippek['.$i.']" placeholder="1...90" 
            value="' . (isset($tippek)? $tippek[$i]:'') . '">';//mező, érték visszaírással a postból
    if (isset($hiba['tippek'][$i])) {//ha van hiba kiírjuk
        $form .= $hiba['tippek'][$i];//hibaüzenet
    }
    $form .= '</label>';//cimke vége

}//mező ciklus vége

$form .= '<br><input type="submit" name="submit" value="tippelek">';//submit
$form .= '</form>';//form close

echo $form;