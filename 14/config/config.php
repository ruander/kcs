<?php
//általános konfigok, beállítások
$secret_key = 'S3cr3t_K3Y!';
//menüpontok
$menuItems = [
    1 => [
        'title' => 'Vezérlőpult',
        'submenu' => false,
        'moduleName' => 'dashboard',
        'icon' => 'fa-dashboard',
    ],
    2 => [
        'title' => 'Adminisztrátorok',
        'submenu' => false,
        'moduleName' => 'admins',
        'icon' => 'fa-user',
    ],
    3 => [
        'title' => 'Cikkek',
        'submenu' => false,
        'moduleName' => 'articles',
        'icon' => 'fa-edit',
    ],
    4 => [
        'title' => 'Kuponjaim',
        'submenu' => false,
        'moduleName' => 'coupons',
        'icon' => 'fa-edit',
    ],
    5 => [
        'title' => 'sorsolás',
        'submenu' => false,
        'moduleName' => 'lots',
        'icon' => 'fa-edit',
        'privilege' => 'hgy@iworkshop.hu',//ő sorsolhat
    ],
];
//modulerendszer paraméterek
$moduleDir = 'modules/';
$moduleExt = '.php';
