<?php
//beléptetés
function login($userdata = []){
    global $link,$secret_key;
    $email = $userdata['email'];

    $password = $userdata['password'];
    //var_dump($password);
    //beragadt bejelentkezések törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' ");
    //felh lekérése email alapján ha statusa 1
    $qry = "SELECT id,username,password,email FROM admins WHERE 
            email= '$email' and 
            `status` = 1
            LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));

    $row = mysqli_fetch_assoc($result);
    if ($row) {
        //bcryptes jelszó ellenőrzés
        if(password_verify($password,$row['password'])) {
            //felhasználó azonosítása a mf azonosítás alapján történik
            $sid = session_id();
            //mf jelszó
            $spass = md5($row['id'] . $secret_key . $sid);
            //ekkor történt:
            $stime = time();
            $_SESSION['userdata'] = $row;
            //eltároljuk az érvényes munkafolyamat adatokat
            $qry = "INSERT INTO sessions(sid,spass,stime) 
            VALUES('$sid','$spass' ,$stime)  ";
            mysqli_query($link, $qry) or die(mysqli_error($link));

            return true;
        }else{
            return false;
        }
    }
    //nem kell visszatérni, mert innentől az auth azonosít
}

//érvényes belépés ellenőrzése
function auth()
{
    global $link, $secret_key;//ezeket látni kell az eljárásnak
    $time_limit = time() - 15 * 60;//15 perc
    $qry = "SELECT spass FROM sessions WHERE 
            sid = '" . session_id() . "'  
            and stime > $time_limit
            LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);// [0] kulcson lesz az spass

    if ($row) {//találtam session id alapján rekordot
        //spass ujragenerálása
        $spass = md5($_SESSION['userdata']['id'] . $secret_key . session_id());
        if ($spass === $row[0]) {
            //stime update
            mysqli_query($link,
                "UPDATE sessions SET stime = '".time()."' 
                      WHERE sid = '".session_id()."'");
            return true;
        }
    }
    return false;
}
//hiba kiírásra
function hibaKiir($key, &$hiba){//hiba változó referenciaátadása
    //ha nem létezik a hiba tömb, (array_key__existnek az kell!) akkor létrehozunk üreset, ha létezik akkor nem változtatjuk
    $hiba = $hiba?:[];
    //ha létezik az adott kulcs akkor visszaadjuk a rajta található értéket
    if(array_key_exists($key,$hiba)){
        return $hiba[$key];
    }
    return;
}
function getInputValue($name, $row = [])
{//fallback: post,db érték, semmi

    $post_input = filter_input(INPUT_POST, $name);

    if ($post_input !== NULL) {
        return $post_input;
    }
    //ha nincs post, nézzük a db-t, ha a rownak van megfelelő kulcsa akkor jött adat a db-ből
    if (array_key_exists($name, $row)) {
        return $row[$name];
    }
    //egyébként meg semmi
    return;
}

//megjelenítéshez '-' hozzáadása a tokenhez
function hyphenate($str)
{
    return implode("-", str_split($str, 6));
}