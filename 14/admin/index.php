<?php
require_once '../config/connect.php';//db csatlakozás ($link)
require_once '../config/config.php';//beállítások és egyebek
require_once '../config/functions.php';//saját eljárások
session_start();//munkafolyamat indítása
//url paraméterek kinyerése:
$o = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:1;//url paraméter kinyerése hogy melyik oldalon állunk épp
$url = '?p='.$o;
//var_dump($_SESSION,session_id());
//kiléptetünk ha kell
if(filter_input(INPUT_GET,'logout') !== null){
    //sessions sor törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' ");
}
$auth = auth();//azonosítás
if(!$auth) {
    header('location:login.php');
    exit();
}
echo '<div>Üdvözöllek '.$_SESSION['userdata']['username'].'! | <a href="?logout">kilépés</a></div>';

//navigáció betöltése:
include ('includes/navigation.inc');

//modul betöltése


//ha létezik ilyen elemünk
if(array_key_exists($o,$menuItems)) {
    $moduleName = $menuItems[$o]['moduleName'].$moduleExt;
    if(file_exists($moduleDir.$moduleName)) {
        include($moduleDir.$moduleName);
    }
}