<?php
$output = '<h2>Sorsolás</h2>';
$today = date('Y-m-d');
//sorsolás lehetőség
$qry = "SELECT * FROM lots WHERE lotDate = '$today'";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
$row = mysqli_fetch_row($result);
//var_dump($row);
if ($row) {
    $output .= 'A mai napon volt már sorsolás...';
} else {
    //nem volt még sorsolás, sorsoljunk
    $winnings = [
        1 => 1,
        2 => 3,
        3 => 50,
    ];
    //lehetséges nyertesek:
    $qry = "SELECT token FROM coupons WHERE wins = ''";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $availableTokens = mysqli_fetch_all($result);
    if (count($availableTokens) < array_sum($winnings)) {//nincs elég, generálunk 200at a létező userekhez
        //lehetséges useridk:
        $users = mysqli_query($link, "SELECT id FROM admins") or die(mysqli_error($link));
        $resUsers = mysqli_fetch_all($users);
        $userIds = [];
        foreach ($user = $resUsers as $userId) {
            array_push($userIds, $userId[0]);
        }
        //mehet a generálás
        for ($i = 1; $i <= 200; $i++) {
            $randomId = $userIds[array_rand($userIds)];
            $token = generateRandomTokensForUsers();
            //ha volt már nem csinálunk semmit, visszalptetjük a ciklusszámlálót
            $qry = "SELECT * FROM coupons WHERE token = '$token' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_row($result);
            if ($row[0] !== NULL) {//update miatt módosítottuk hogy a saját emailje ne dobjon hibát
                $i--;
            } else {//nem volt még ilyen mehet db be
                $data = [
                    'token' => $token,
                    'user_id' => $randomId
                ];
                $qry = "INSERT INTO coupons(`" . implode('`,`', array_keys($data)) . "`) VALUES('" . implode("','", $data) . "')";//összeállítjuk az insertet a data segédtömb eleminek segítségével
                mysqli_query($link, $qry) or die(mysqli_error($link));
            }
        }
        //lehetséges nyertesek újra lekérése:
        $qry = "SELECT token FROM coupons WHERE wins = ''";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $availableTokens = mysqli_fetch_all($result);
    }

    //van elég coupon mehet a sorsolás
    foreach($winnings as $winningType => $available){
        //kiveszük a nyerteseket és tároljuk a lehetséges couponok közül

        for($i=1;$i<=$available;$i++){
            //mindig a 0 kulcs nyer és kikerül
            $winKey = array_rand($availableTokens);
            $winners[$winningType][] = $availableTokens[$winKey];
            unset($availableTokens[$winKey]);
        }
    }
    //nyertesek mentése
    foreach($winners as $winningType => $coupons){
        foreach($coupons as $token){
            $qry = "UPDATE coupons SET wins = $winningType WHERE token = '{$token[0]}' LIMIT 1";
            mysqli_query($link,$qry) or die(mysqli_error($link));
        }
    }
    //sorsolás mentése
    $week = date("W", strtotime($today));
    mysqli_query($link,"INSERT INTO lots(lotDate,week) VALUES('$today','$week')") or die(mysqli_error($link));
}
echo $output;

//random token generátor
function generateRandomTokensForUsers()
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 24; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}