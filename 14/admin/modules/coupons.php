<?php
//includeok az indexben vannak: config fnctions,connect
$action = filter_input(INPUT_GET, 'act') ?: 'list';//művelet, vagy sikerül filterezni, vagy list
$t_id = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);// id ha van
$dbTable = 'coupons';
//var_dump($action, $t_id);
$coupon_uploaded = '';
//post ellenőrzés ha kell
if (!empty($_POST)) {
    $hiba = [];
    $coupon_uploaded = filter_input(INPUT_POST, 'coupon');

    $test = preg_match("([a-zA-Z0-9]+[-])", $coupon_uploaded, $coupon);
    $coupon = strtoupper(str_replace('-', '', $coupon_uploaded));
    //var_dump($coupon, $test);
    if ($test != 1 || strlen($coupon) != 24) {
        $hiba['coupon'] = '<span class="error">Hibás kuponkód!</span>';
    } else {
        //coupon token unique!
        $qry = "SELECT * FROM $dbTable WHERE token = '$coupon' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if ($row[0] != $t_id && $row[0] !== NULL) {//update miatt módosítottuk hogy a saját emailje ne dobjon hibát
            $hiba['coupon'] = '<span class="error">Már feltöltött kuponkód!</span>';
        }
    }

    if (empty($hiba)) {//nincs hiba
        $data = [
            'token' => $coupon,
            'user_id' => $_SESSION['userdata']['id']
        ];
        //var_dump($data);

        //10% win esély
        if (rand(1, 100) <= 10) {
            $data['wins'] = 'Termék nyeremény';
        }

        $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($data)) . "`) VALUES('" . implode("','", $data) . "')";//összeállítjuk az insertet a data segédtömb eleminek segítségével


        //query futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        header('location:' . $url . '&act=list');
        exit();
    }
}
$output = '';//ez lesz a modul kimenete

//uj kupon mező
$row = isset($row) ? $row : [];
//űrlap összeállítása
$form = '<form method="post">
    <div class="form-group">
     <div id="coupon-info"></div>
        <label>Kuponkód<sup>*</sup> 24 karakter:
         <input style="text-transform: uppercase;" maxlength="27" size="30" type="text" id="tbNum" onkeyup="addHyphen(this)"
            placeholder="ABCDEF-GHIJKL-MN1234-OP5678" name="coupon" value="' . getInputValue('coupon', $row) . '">' . hibaKiir('coupon', $hiba) .
    '</label>
    </div>
    
    <button>Kuponkód felvitele</button>
</form>';
$output .= $form;
//listázás
$qry = "SELECT * FROM $dbTable WHERE user_id = " . $_SESSION['userdata']['id'];
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
//tábla kezdés és új felvitel 'gomb'
$output .= '<table>';
//fejléc
$output .= '<tr>';
$output .= "<th>Kód</th>"
    . "<th>Nyeremény</th>"
    . "<th>Dátum</th>";
$output .= '</tr>';
//adatsorok
foreach ($rows as $couponCode) {
    $output .= '<tr>';
    $output .= "<td>" . hyphenate($couponCode['token']) . "</td>"
        . "<td>{$couponCode['wins']}</td>"
        . "<td>{$couponCode['time_created']}</td>";
    $output .= '</tr>';
}
$output .= '</table>';
//javascript ideiglenes helye
$output .= "<script>
	function addHyphen (element) {
        let ele = document.getElementById(element.id);
        let myEl = ele;//későbbre
        
        if(ele.value.length > 6){//ha elég hosszu a string csak akkor babráljuk
                ele = ele.value.split('-').join('');    // Remove dash (-) if mistakenly entered.
            let finalVal = ele.match(/.{1,6}/g).join('-');
                myEl.value = finalVal;
        }
        
        //AJAX kérés összeállítása és a válasz feldolgozása
        //feladat 1: írja ki a begépelt karakterek számát ( - nélkül) amig nincs 24
        //feladat 2: nem megengedett karaktert ne tudjak begépelni, azaz azonnal távolítsa is el
        if(ele.length == 24){//megvan a kód, mehet ellenőrzésre
            let http = new XMLHttpRequest();//objektum 
            let url = 'add_coupon.php';
            let params = 'code='+ ele;
            http.open('POST', url, true);
            
            //helyes fejléc információk beállítása a kéréshez
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            
            http.onreadystatechange = function() {//állapotváltozáshoz eljárás
                if(http.readyState == 4 && http.status == 200) {//jön a válasz
                    //ha hiba van (saját válaszból, ha a state hibás akkor más feldolgozás
                    let response = JSON.parse(http.responseText);
                    if(response.errors){
                        //hiba van
                    }else{
                       //nincs hiba ürítjük a kódot
                       myEl.value = '';
                    }
                    document.querySelector('#coupon-info').innerHTML = response.responseText;
                    console.log(response);
                }else{
                    console.log(http.statusText);//hiba ha status nem 200
                }
            }
            http.send(params);  //kérés küldése
        }else{
            //ürítsük az info-t
            document.querySelector('#coupon-info').innerHTML = '';
        }
    }
</script>";
//echo '<pre>' . var_export($rows, true) . '</pre>';

echo $output;//kimenet kiírása egy lépésben

