<?php
require_once '../config/connect.php';//db csatlakozás ($link)
require_once '../config/config.php';//beállítások és egyebek
require_once '../config/functions.php';//saját eljárások
session_start();//munkafolyamat indítása
$auth = auth();
if ($auth == true) {
    header('location:index.php');
    exit();
}
$msg = '';
if(!empty($_POST)){//urlapról beléptetés
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
    $password = filter_input(INPUT_POST,'pass');
    
    if(login(['email'=>$email,'password'=>$password])){
        header('location:index.php');
        exit();
    }else{
        $msg = '<div class="error">Nem megfelelő email/jelszó páros!</div>';
    }
}

$form = '<form method="post">';
if($msg != ""){
    echo $msg;
}
$form .= '<div class="form-group">
        <label>Email<sup>*</sup>:
            <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST,'email') . '"></label>
    </div>
    <div class="form-group">
        <label>Jelszó<sup>*</sup>:
            <input type="text" name="pass" value=""></label>
    </div>
    <button>belépés</button>
</form>';

echo $form;
