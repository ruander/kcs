<?php
//navigáció összeállítása a config -ban felvette tömb felhasználásával
$nav = '<ul>';
foreach($menuItems as $menuId => $item) {
    if (!isset($item['privilege'])
        OR
        (isset($item['privilege'])
            and
            $item['privilege'] == $_SESSION['userdata']['email'])
    ) {
        $nav .= '<li class="fa ' . $item['icon'] . '"><a href="?p=' . $menuId . '">' . $item['title'] . '</a></li>';
    }
}
$nav .='</ul>';

echo '<nav>'.$nav.'</nav>';
// /navigació