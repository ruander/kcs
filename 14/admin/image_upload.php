<?php
session_start();
//var_dump($_SERVER);
if (!empty($_POST)) {
    $hiba = [];
    //a feltöltött file adatai a $_FILES tömben találhatóak az [input mező neve] indexen
    $fileData = $_FILES['file_to_upload'];
    //a type kiterjesztés alapú, ezért 'átverhető'
    //a feltöltött file ideiglenesen a tmp_name kulcson található helyre kerül
    //egyszerű filefeltöltés
    //var_dump($fileData);
    if ($fileData['error'] != 0 or !is_uploaded_file($fileData['tmp_name'])) {
        $hiba['file_to_upload'] = 'hiba a feltöltés során!';
    } else {
        //méret ellenőrzés (byte) max 2MB
        $sizeLimit = 15 * 1024 * 1024;
        if ($fileData['size'] > $sizeLimit) {
            $hiba['file_to_upload'] = "Túl nagy fileméret!";
        }
        //tipus ellenőrzés
        $validFileTypes = [
            'image/jpeg', 'text/plain', 'application/pdf'
        ];//ezeket engedjük
        if (!in_array($fileData['type'], $validFileTypes)) {//ha nincs az engedélezett tömbben, akkor hiba
            $hiba['file_to_upload'] .= " nem megfelelő filetípus";
        }
    }
    if (empty($hiba)) {
        //KÉPMŰVELETEK
        if ($imageInfo = getimagesize($fileData['tmp_name'])) {//https://www.php.net/manual/en/function.getimagesize.php
            //var_dump($imageInfo);
            //eredeti képméret sima move uploaded el megoldható, de ...
            //méretarányos kicsinyítés lépései
            $origWidth = $imageInfo[0];
            $origHeight = $imageInfo[1];
            //egy max 600x600px helyre tervezzük tehát a nagyobb méretet beállítjuk 600 ra és a kisebbet számoljuk
            $ratio = $origWidth / $origHeight;
            if ($ratio > 1) {//fekvő kép
                $width = 600;
                $height = round($width / $ratio);

            } else {//álló kép
                $height = 600;
                $width = round($height * $ratio);
            }
            //echo "új méret: $width / $height";
            //a képeket az images mappába gyűjtjük, a többit az uploadsba
            //eredeti kép memóriába: TÍPUSFÜGGŐ!!!!
            $im = imagecreatefromjpeg($fileData['tmp_name']);
            imagejpeg($im,'images/'.$fileData['name'],85);//eredeti kép kiírása images be eredeti néven
            //vászon a méretarányos kicsinyítésnek:
            $canvas = imagecreatetruecolor($width,$height);
            imagecopyresampled(
                $canvas, $im,
                0, 0, 0, 0,
                $width, $height, $origWidth, $origHeight) ;
            //header beállítás hogy kép vagyunk
            //header('Content-Type: image/jpeg');
            imagejpeg($canvas,'images/mid-'.$fileData['name'],100);//kicsinyített kiírása images mappába mid- prefixel
            //bélyegkép elkészítése:
            $thumbWidth = $thumbHeight = 150;
            $thumbCanvas = imagecreatetruecolor($thumbWidth,$thumbHeight);
            //itt a kisebb méretre kell méretarányosan kicsinyiteni
            if($ratio > 1){
                $height = $thumbHeight;
                $width = $height * $ratio;
                $x_offset = ($width-$thumbWidth)/2;
                $y_offset = 0;
            }else{
                $width = $thumbWidth;
                $height = $width / $ratio;
                $x_offset = 0;
                $y_offset = ($height-$thumbHeight)/2;
            }
            imagecopyresampled(
                $thumbCanvas, $im,
                0, 0, $x_offset, $y_offset,
                $width, $height, $origWidth, $origHeight) ;
            //header('Content-Type: image/jpeg');
            imagejpeg($thumbCanvas,'images/thumb-'.$fileData['name'],60);//thumbnail kiírása
            header('location:'.$_SERVER['PHP_SELF']);exit();
            exit();
            //képfeltöltés rész vége, ilyenkor nincs filefeltöltés mód
        }

        $targetDir = 'uploads/';
        // áű 201905.10$ (jo buli volt) .jpg -> au-201905-10---jo-buli-volt--.jpg
        $origName = $fileData['name'];
        $nameParts = array_reverse(explode('.', $origName));
        $ext = $nameParts[0];
        unset($nameParts[0]);//kiszedjük a kiterjesztést a tömbből
        $origNameWOExtension = implode('.', array_reverse($nameParts));
        //die($origNameWOExtension.'|'.$ext);
        //filenév ékezettelenítése - HF
        if (!is_dir($targetDir)) mkdir($targetDir, 0644, true);//ha nincs mappa, készítsük el
        move_uploaded_file($fileData['tmp_name'], $targetDir . $fileData['name']);//helyezzük át a filet a végleges helyére az eredeti nevén
        //header('location:'.$_SERVER['PHP_SELF']);exit();
    }
}
?>
<form method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo session_id(); ?>">
    <label>File feltöltés: <input type="file" name="file_to_upload"></label>
    <?php
    if (isset($hiba['file_to_upload'])) {
        echo $hiba['file_to_upload'];//ha van hiba írjuk ki
    }
    ?>
    <br>
    <br>
    <button type="submit">mehet</button>
</form>
