<?php
//pure php file
/***beágyazott ciklusok***/
echo 'Helo';
//sakktábla
//a kiírandó stringeket egy változóba gyűjtjük
$output = '<table class="sakk" border="1">';
//ciklus a sorok létrehozására
for($j=1;$j<=8;$j++){
	$output .= '<tr>';
	//ciklus a cellák létrehozására
	for($i=1;$i<=8;$i++){
		$output .=	'<td>'.$j.'&nbsp;'.$i.'</td>';
	}
	$output .= '</tr>';
}
$output .= '</table>';// operátor $a .= 'b' -> $a = $a .'b'
//kiírás egy lépésben
echo $output;
//stílus a sakktáblához
echo $style = '<style>
.sakk tr:nth-child(2n) td:nth-child(2n),
.sakk tr:nth-child(2n-1) td:nth-child(2n-1)
{
		background:#000;
}
</style>';
//x*y tábla létrehozása
$sor = 8;
$oszlop = 8;
//table ujragenerálása az outputba
$output = '<table class="sakk" border="1">';
$j = 1;
while($j<=$sor){//ciklus a soroknak
	$output .= '<tr>';
	$i = 1;//belső ciklusváltozó visszaállítása
	while($i<=$oszlop){
		$output .=	'<td>'.$j.'&nbsp;'.$i.'</td>';
		$i++;
	}
	$output .= '</tr>';
	$j++;
}
$output .= '</table>';
echo $output;//kiírás egy lépésben

//minta adattömb
$menu = [
	'home' => 'Kezdőlap',
	'about' => 'Rólunk',
	'services' => 'Szolgáltatások',
	'contact' => 'Kapcsolat',
];
//menü 'kirajzolása'
$menuHTML = '<nav><ul>';
//ciklus a menüpontoknak
foreach($menu as $menuSlug => $menuTitle){
 $menuHTML .='<li><a href="?'.$menuSlug.'">'.$menuTitle.'</a></li>';
}
$menuHTML .= '</ul></nav>';
echo $menuHTML;
$anotherMenu = [
	1 => [
		'title' => 'Kezdőlap',
		'slug' => 'home',
		'submenu' => false,
		'icon' => 'fa fa-home',
	],
	2 => [
		'title' => 'Rólunk',
		'slug' => 'about',
		'submenu' => false,
		'icon' => 'fa fa-user',
	],
	3 => [
		'title' => 'Szolgáltatások',
		'slug' => 'services',
		'submenu' => [
			1 => [
				'title' => 'Oktatás',
				'slug' => 'education',
				'submenu' => false,
				'icon' => 'fa fa-item1',
			],
			2 => [
				'title' => 'Tanácsadás',
				'slug' => 'consultancy',
				'submenu' => false,
				'icon' => 'fa fa-item2',
			],
			3 => [
				'title' => 'Refaktorálás',
				'slug' => 'refactoring',
				'submenu' => false,
				'icon' => 'fa fa-item3',
			],			
		],
		'icon' => 'fa fa-search',
	],
	4 => [
		'title' => 'Kapcsolat',
		'slug' => 'contact',
		'submenu' => false,
		'icon' => 'fa fa-email',
	],
];