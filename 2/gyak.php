<?php
//gyakorlás
$tomb = [1,3,14];
// tömb viselkedése és tömb műveletek
//echo $tomb;//Array, az elemeiről semmi info nincs notice-t okoz!
//változó tipusa
echo gettype($tomb);
//tömb tipus vizsgálata
if(is_array($tomb)){//tömb
	echo '<br>változó tömb';
}else{
	echo '<br>a változó:'.gettype($tomb);
}
foreach($tomb as $k => $v){
		echo '<br>'.$v;
}
//újabb dimenzió
$tomb[] = [
	'errors' => [28,121]
];
echo '<pre>';
//var_dump($tomb);
foreach($tomb as $k => $v){
	if(!is_array($v)){// operátor ! -> negálás, azaz az ellenkezőjére váltja AZ ŐT KÖVETŐ KIFEJEZÉST: (!false -> true
		echo '<br>'.$v;
	}else{//az érték tömb így újra bejárjuk
		foreach($v as $a => $b){
			echo '<br>&nbsp;'.$b;
		}
	}			
}
//rekurzivítás bemutatása a tömb minden szntjének minden értéke kiíratásával
tombKiiras($tomb);

function tombKiiras($mixed){
	//a kapott paraméter vizsgálata
	if(is_array($mixed)){//tömböt kaptunk, bejárjuk
		foreach($mixed as $k => $v){
			//ha az aktuális elem tömb akkor be kell járni és kiírni az értéket ha primitív ha tömb akkor ......
			if(is_array($v)){
				tombKiiras($v);
			}else{
				echo '<br>'.$v;
			}
		}
	}else{//a kapott paraméter nem tömb ezért kiírjuk
			echo '<br>'.$mixed;
	}
	//var_dump($mixed);
}